package com.tp.collections;

import java.util.HashSet;
import java.util.Iterator;


public class Set {
	public static void main(String[] args) {
		HashSet hs = new HashSet();
		hs.add("toto");
		hs.add(12);
		hs.add('d');
		//hs.remove(12);
		
		Iterator it = hs.iterator();
		while(it.hasNext())
				System.out.println(it.next());
		contains(12);
		
		System.out.println("\nParcours avec un tableau d'objet");
		System.out.println("-----------------------");
		
		Object[] obj = hs.toArray();
		for (Object o : obj)
			System.out.println(o);
		
		
	}

}
